<?php

declare(strict_types=1);

namespace C33s\Bundle\MakerExtraBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class C33sMakerExtraExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
//          $config = $this->processConfiguration($configuration, $configs);
        $this->processConfiguration($configuration, $configs);

//        $loader = new Loader\PhpFileLoader($container, new FileLocator(__DIR__.'/../../config'));
//        $loader->load('services.php');

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yaml');
    }
}
