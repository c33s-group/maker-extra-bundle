<?php

declare(strict_types=1);

namespace C33s\Bundle\MakerExtraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class C33sMakerExtraBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
