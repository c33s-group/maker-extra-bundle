<?= "<?php\n"; ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use C33s\Bundle\EntityLoaderBundle\EntityLoader\BaseContent;
use DateTimeImmutable;

final class <?= $class_name; ?>  extends BaseContent<?= "\n"; ?>
{
    public static function createdAtDate(): DateTimeImmutable
    {
        return new DateTimeImmutable('<?= (new DateTimeImmutable())->format('c'); ?>');
    }

//    public function preventLoadBeforeDate(): DateTimeInterface
//    {
//        return new DateTimeImmutable('<?= (new DateTimeImmutable())->format('Y-m-d H:i:s'); ?>');
//    }

//    public function preventLoadAfterDate(): DateTimeInterface
//    {
//        return new DateTimeImmutable('<?= (new DateTimeImmutable())->format('Y-m-d H:i:s'); ?>');
//    }

    public function getEntities(): array
    {
        $entities = [];

        //$entities[] = new ExampleEntry();

        return $entities;
    }
}
