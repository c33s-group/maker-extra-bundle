<?php

declare(strict_types=1);

// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// phpcs:disable PSR1.Files.SideEffects
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable Generic.Files.LineLength.TooLong
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass
// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
// phpcs:disable Generic.CodeAnalysis.EmptyStatement.DetectedCatch
// phpcs:disable Generic.Formatting.SpaceBeforeCast.NoSpace

const C33S_SKIP_LOAD_DOT_ENV = true;
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
const C33S_ROBO_DIR = '.robo';

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
(is_dir($roboDir) || mkdir($roboDir) || is_dir($roboDir)) && chdir($roboDir);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === strpos(implode((array) $output), 'Nothing to install')) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use C33s\Robo\BaseRoboFile;
use C33s\Robo\C33sExtraTasks;
use C33s\Robo\C33sTasks;
use Consolidation\AnnotatedCommand\CommandData;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends BaseRoboFile
{
    use C33sTasks;
    use C33sExtraTasks;

    protected array $portsToCheck = [];

    // phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass

    /**
     * @hook pre-command
     */
    public function preCommand(CommandData $commandData): void
    {
        if ($this->isEnvironmentCi()) {
            $this->input()->setInteractive(false);
        }

        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '2.0.14',
            'php-cs-fixer' => 'v2.16.1',
            'phpcs' => '3.5.3',
            'composer-unused' => '0.7.5',
        ]);
    }

    // phpcs:enable

    /**
     * Initialize project.
     */
    public function init(): int
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }
        $this->update();

        return 0;
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = ''): void
    {
        $this->checkCs($arguments);
        $this->checkCsfixer($arguments);
        $this->checkPhpstan($arguments);
        $this->checkUnused($arguments);
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = ''): int
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .robo/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }

        return 0;
    }

    /**
     * Update the Project.
     */
    public function update(): int
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->_execPhp('php ./.robo/bin/composer.phar install --no-progress --ansi --no-suggest --prefer-dist --optimize-autoloader');
        } else {
            $this->_execPhp('php ./.robo/bin/composer.phar install');
        }

        return 0;
    }
}
